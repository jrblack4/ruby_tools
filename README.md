# ruby_tools
Tools built in ruby to support test automation development.

# Assertion Builder
Live in line recomended assertions from a working REST API request. 
Parses the response body and examines the value type.
Based off this information, it makes assumptions on how to assert the value.

If the value is a JSON object, it creates a snake_case variable out of the KeyName.
and recursively parses that section of the response using the snake_case'd key_name
as the parent variable. 

Ex...
 ```
 my_obj = {
 "Foo": "bar",
 "String": "it is",
 "MyObject": {
    "HasPropertyOne": 1,
    "HasPropertyTwo": "Two"
  }
}

 Tools::Api.build_assertion(response: my_obj)
  ###Recommended Assertions###
expect(response.body['Foo']).to eq('bar')
  expect(response.body['String']).to eq('it is')
	
  my_object = response.body['MyObject']
  expect(my_object['HasPropertyOne']).to eq(1)
  expect(my_object['HasPropertyTwo']).to eq('Two')
  ###End Recommended Assertions###
```

# Swagger Parse
Navigates the pages and/or json information of an API's swagger docs to generate a list of end points.


# Swagger Test Generator
The next step from `Swagger Parse`.
It takes the information from Swagger Parse to then output to files and generating a template for the base files for an entire test suite.
